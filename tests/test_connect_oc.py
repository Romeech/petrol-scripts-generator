from actions.oc2pc import prepare_oc, prepare_pc


def test_prepare_oc_exits():
    prepare_oc()


def test_prepare_pc_exists():
    prepare_pc()


def test_prepare_oc_returns_script_body():
    body = prepare_oc()
    assert isinstance(body, str)


def test_prepare_oc_for_existing_oc():
    body = prepare_oc(2001)
    assert body == "UPDATE oc_commercial_parameters SET xxx = 1"
